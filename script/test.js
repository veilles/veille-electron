const {ipcRenderer, dialog} = require('electron')
const fetch = require("node-fetch");

// ssl 
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
const btn = document.getElementById('fetch')

//fetch
btn.addEventListener('click', async function () {
    console.log("click")
    const resp = await getQuestions()
    let root = document.getElementById("root")
    let ul = document.createElement("ul")
    root.appendChild(ul)
    resp.forEach(element => {
        console.log(element)
        let li = document.createElement("li")
        let content = document.createTextNode(element.questionEn + element.questionFr + element.difficulty)
        li.appendChild(content)
        ul.appendChild(li)
    });
})

async function getQuestions() {
    try {
      const resp = await fetch("https://127.0.0.1:8000/api/" + 'quiz', {
        method: 'GET'
      })
      const data = await resp.json()
      return data
  
    } catch (e) {
      console.error(e)
    }
}

//ipc 
const devtool = document.getElementById('ipcTest')
devtool.addEventListener('click', function(){
    const resp = ipcRenderer.sendSync(/*eventName*/'test',/*argument*/"devtool")
    console.log(resp)
    //dialog.showErrorBox('Error box', 'salutation')
})